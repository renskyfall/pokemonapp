var express = require('express');
var router = express.Router();
var getPokemon = require('./pokemonFunc')
var pg = require('pg')

const config = {
    user: "postgres",
    password: "majubersama",
    database: "pokemon",
    host: "localhost",
    port: 5433
}
/* GET users listing. */

router.get('/', async function(req, res, next) {

    const pool = new pg.Pool(config);
    pool.connect((err, client, done) => {
        if (err) throw err;
        client.query('SELECT * FROM pokemons order by pokemon_id', (err, res2) => {
            if (err)
                console.log(err.stack);
            else {
                console.log(res2.rows);
                res.send(res2.rows)
            }
            pool.end()
        })
    })
    
});

router.get('/:id', async function(req, res, next) {
    var myPokemon = {}
    var nickname

    await getPokemon.getMyPokemonDetail(req.params.id, res, next,
        function(tempVal){

            nickname = tempVal.nickname
            console.log("nickname ="+nickname)

            getPokemon.getDetailPokemonById(req.params.id, res, next,
                function(tempVal2){
                    if(nickname !== undefined){
                        myPokemon = tempVal2
                        myPokemon.nickname = nickname
                     
                    }
                    res.send(myPokemon)
                },
                function(error2){
                    res.send(error2)
            })
            
        },
        function(error){
            res.send(error)
        })
    
    
        
    
});

router.post('/catchPokemon', async function(req, res, next) {
    var id = req.body.id
    var nickname = req.body.nickname
    var myResponse = {}
    var message
    var isCatch = Math.random() < 0.5

    if(isCatch){
        const pool = new pg.Pool(config);
        pool.connect((err, client, done) => {
        if (err) throw err;
        client.query(`INSERT INTO pokemons (pokemon_id, nickname) VALUES (${id}, '${nickname}')`, (err, res2) => {
            if (err)
                console.log(err.stack);
            else {
                console.log(res2);
                message = "Success Catch Pokemon"
                myResponse.isCatch = isCatch
                myResponse.message = message
                res.send(myResponse)
            }
            pool.end()
        })
    })
    }else{
        message = "Failed Catch Pokemon"
        myResponse.isCatch = isCatch
        myResponse.message = message
        res.send(myResponse)
    }
    
    
    
});

router.delete('/releasePokemon/:id', async function(req, res, next) {
    var id = req.params.id
    var myResponse = {}
    var message
    const randomNumber = Math.floor(Math.random() * 500) + 1
    let numIsPrime = isPrime(randomNumber);

    if(numIsPrime){
        const pool = new pg.Pool(config);
        pool.connect((err, client, done) => {
        if (err) throw err;
            client.query(`DELETE FROM pokemons WHERE pokemon_id= ${id};`, (err, res2) => {
                if (err)
                    console.log(err.stack);
                else {
                    console.log(res2);
                    
                    message = "Success Release Pokemon"
                    myResponse.message = message
                    myResponse.primeNumber = randomNumber
                    myResponse.isPrime = numIsPrime
                    res.send(myResponse)
                }
                pool.end()
            })
        })
    }else{
        message = "Failed Release Pokemon"
        myResponse.message = message
        myResponse.primeNumber = randomNumber
        myResponse.isPrime = numIsPrime
        res.send(myResponse)
    }
    
    
    
    
    
});

router.patch('/renamePokemon/:id', async function(req, res, next) {
    var id = req.params.id
    var myResponse = {}
    var nickname
    var message

    await getPokemon.getMyPokemonDetail(id, res, next,
        function(tempVal){
            nickname = tempVal.nickname
            const myArr = nickname.split("-");
            const pool = new pg.Pool(config);
            var newNum = fibonacci(tempVal.counter)
            var newNickname = req.body.nickname+'-'+newNum
            pool.connect((err, client, done) => {
            if (err) throw err;
                client.query(`Update pokemons set nickname = '${newNickname}', counter = counter + 1 WHERE pokemon_id= ${id};`, (err, res2) => {
                    if (err)
                        console.log(err.stack);
                    else {
                        console.log(res2);
                        message = "Success Rename Pokemon"
                        myResponse.message = message
                        myResponse.nickname = newNickname
                        res.send(myResponse)
                    }
                    pool.end()
                })
            })
        },
        function(error){
            res.send(error)
    })

    
    
});

function isPrime(num) {
    for(var i = 2; i < num; i++)
    if(num % i === 0) return false;
    return num > 1;
}

function fibonacci(myNum){
    let n1 = 0, n2 = 1, nextTerm;


    for (let i = 1; i <= myNum; i++) {
        nextTerm = n1 + n2;
        n1 = n2;
        n2 = nextTerm;
    }
    return n2
}

module.exports = router;

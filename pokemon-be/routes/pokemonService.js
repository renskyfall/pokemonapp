const axios = require('axios');
var express = require('express');
var router = express.Router();
var getPokemon = require('./pokemonFunc')

router.get('/', async function(req, res, next) {
    
    var contoh = axios.get('https://pokeapi.co/api/v2/pokemon')
    .then(function (response) {
        // handle success
        console.log(response.data);
        return response.data
       
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    })
    res.send(await contoh);
  
});

router.get('/:id', async function(req, res, next) {
    
    getPokemon.getDetailPokemonById(req.params.id, res, next, 
        function(tempVar){
            res.send(tempVar)
        }, 
        function(error){
            res.send(error)
        })
    
});

module.exports = router;

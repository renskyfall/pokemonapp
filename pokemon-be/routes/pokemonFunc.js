const axios = require('axios');
var pg = require('pg')
var image_url = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'
const config = {
    user: "postgres",
    password: "majubersama",
    database: "pokemon",
    host: "localhost",
    port: 5433
}

exports.getDetailPokemonById = function(id, res, next, callback, fallout){
    var pokemon = {} 
    return axios.get('https://pokeapi.co/api/v2/pokemon/' + id)
    .then(function (response) {
        // handle success
        console.log(response.data);
        
        pokemon.id = response.data.id
        pokemon.name = response.data.name
        pokemon.height = response.data.height
        pokemon.weight = response.data.weight
        pokemon.types = response.data.types
        pokemon.moves = response.data.moves
        pokemon.imageUrl = image_url+pokemon.id+'.png'
        callback(pokemon)
       
    })
    .catch(function (error) {
        // handle error
        fallout(error)
        console.log(error);
    })
}

exports.getDetailPokemonByUrl = function(url, res, next, callback, fallout){
    var pokemon = {} 
    return axios.get(url)
    .then(function (response) {
        // handle success
        console.log(response.data);
        
        pokemon.id = response.data.id
        pokemon.name = response.data.name
        pokemon.height = response.data.height
        pokemon.types = response.data.types
        pokemon.moves = response.data.moves
        pokemon.imageUrl = image_url+pokemon.id+'.png'
        callback(pokemon)
       
    })
    .catch(function (error) {
        // handle error
        fallout(error)
        console.log(error);
    })
}

exports.getMyPokemonDetail = function(id, res, next, callback, fallout){
    const pool = new pg.Pool(config);
    return pool.connect((err, client, done) => {
        if (err) throw err;
        client.query('SELECT * FROM pokemons where pokemon_id = '+ id, (err, res2) => {
            if (err)
                fallout(err.stack);
            else {
                console.log(res2.rows);
                var pokemon = {}
                if(res2.rowCount > 0){
                    pokemon = res2.rows[0]
                }
                callback(pokemon)
            }
            pool.end()
        })
    })
}
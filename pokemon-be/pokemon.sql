/*
 Navicat Premium Data Transfer

 Source Server         : RenoDB
 Source Server Type    : PostgreSQL
 Source Server Version : 120002
 Source Host           : localhost:5433
 Source Catalog        : pokemon
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120002
 File Encoding         : 65001

 Date: 23/10/2021 10:45:54
*/


-- ----------------------------
-- Table structure for pokemons
-- ----------------------------
DROP TABLE IF EXISTS "public"."pokemons";
CREATE TABLE "public"."pokemons" (
  "pokemon_id" int8 NOT NULL,
  "nickname" text COLLATE "pg_catalog"."default",
  "counter" int8
)
;

-- ----------------------------
-- Records of pokemons
-- ----------------------------
INSERT INTO "public"."pokemons" VALUES (2, 'Kuro-8', 6);
INSERT INTO "public"."pokemons" VALUES (1, 'Sylvana-1', 2);

-- ----------------------------
-- Primary Key structure for table pokemons
-- ----------------------------
ALTER TABLE "public"."pokemons" ADD CONSTRAINT "myPokemon_pkey" PRIMARY KEY ("pokemon_id");

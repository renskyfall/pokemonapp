# pokemon-be

**Generate SQL**
generate _**pokemon.sql**_ to your local Postgres Database

**HOW TO RUN**
```
nodemon
```


**HOW TO TEST THE API**

You can import _**Pokemon Collection.postman_collection.json**_ in your postman and test it after you run the app
